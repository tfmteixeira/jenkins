/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tiagoteixeira
 */
public class KeyChainTest {
    
    private static final String KEYCHAIN_FILE = "Keychain.txt";
    private static final String KEYCHAIN_MASTER_KEY = "#wisper";

    File userKeychainFile = new File(KEYCHAIN_FILE);

    KeyChain keyChainTest;
    
    public KeyChainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of from method, of class KeyChain.
     */
    @Test
    public void testFrom() throws Exception {
        try {
            keyChainTest = KeyChain.from(userKeychainFile, new CipherTool(KEYCHAIN_MASTER_KEY));
        } catch (IOException ex) {
            fail("Failed finding source: " + ex.getMessage());
        }
    }

    /**
     * Test of put method, of class KeyChain.
     */
    @Test
    public void testPut() {

        KeyEntry novaEntrada = new KeyEntry();
        novaEntrada.setApplicationName("test");
        novaEntrada.setUsername("tiago");
        novaEntrada.setPassword("tt");

        try {
            KeyChain teste = KeyChain.from(userKeychainFile, new CipherTool(KEYCHAIN_MASTER_KEY));

            teste.put(novaEntrada);

            KeyEntry result = teste.find("test");

            assertEquals("failed to parse Password", result.getPassword(), "tt");
            assertEquals("failed to parse Password", result.getUsername(), "tiago");

        } catch (IOException ex) {
            fail("Failed finding source: " + ex.getMessage());
        }

    }

    /**
     * Test of find method, of class KeyChain.
     */
    @Test
    public void testFind() {
         try {
            keyChainTest = KeyChain.from(userKeychainFile, new CipherTool(KEYCHAIN_MASTER_KEY));
        } catch (IOException ex) {
            fail("Failed finding source: " + ex.getMessage());
        }
        KeyEntry result = keyChainTest.find("ua");

        assertEquals("failed to parse Password", result.getPassword(), "6rb5rv3n267cjoe5od");
        assertEquals("failed to parse Password", result.getUsername(), "ico");
    }

    /**
     * Test of save method, of class KeyChain.
     */
    @Test
    public void testSave() {
        
    }

    /**
     * Test of allEntries method, of class KeyChain.
     */
    @Test
    public void testAllEntries() {
        
    }

    /**
     * Test of sortedEntries method, of class KeyChain.
     */
    @Test
    public void testSortedEntries() {
        
    }

    /**
     * Test of toString method, of class KeyChain.
     */
    @Test
    public void testToString() {
        
    }
    
}
