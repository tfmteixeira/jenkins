
package superkey.keychain;

import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rui
 */
public class CipherToolTest {

    private static final String KEYCHAIN_FILE = "Keychain.txt";
    private static final String KEYCHAIN_MASTER_KEY = "#wisper";

    File userKeychainFile = new File(KEYCHAIN_FILE);

    CipherTool testarCipher;

    public CipherToolTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of writeProtectedKeychain method, of class CipherTool.
     */
    @Test
    public void testWriteProtectedKeychain() throws Exception {

    }

    /**
     * Test of readProtectedKeychain method, of class CipherTool.
     */
    @Test
    public void testReadProtectedKeychain() throws Exception {

        
    }

    @Test(expected = IOException.class)
    public void testReadProtectedKeychainWrongKey() throws Exception {

        testarCipher = new CipherTool("#wisperBadKEy");

        KeyChain teste = KeyChain.from(userKeychainFile, new CipherTool(KEYCHAIN_MASTER_KEY));

        testarCipher.readProtectedKeychain(teste, userKeychainFile);

    }

}
